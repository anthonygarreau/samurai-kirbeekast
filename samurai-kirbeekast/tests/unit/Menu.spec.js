import { shallowMount, createLocalVue } from '@vue/test-utils'
import Menu from '@/components/Menu.vue'

import Vuex from 'vuex'
const localVue = createLocalVue()
localVue.use(Vuex)

describe('Menu.vue', () => {
  it('Should have logo', () => {
    const wrapper = shallowMount(Menu)
    expect(wrapper.find('.logo img').exists()).toBe(true)
  })

  it('Should have play button', () => {
    const wrapper = shallowMount(Menu)
    expect(wrapper.find('.playButton').exists()).toBe(true)
    expect(wrapper.find('.playButton').text()).toEqual('PLAY')
  })

  it('Should have "Joining..." text on play button', () => {
    const store = new Vuex.Store({
      state: {
        'waitingForRoom': true
      }
    })
    const wrapper = shallowMount(Menu, { store })
    expect(wrapper.find('.playButton').exists()).toBe(true)
    expect(wrapper.find('.playButton').text()).toEqual('Joining ...')
  })
})
