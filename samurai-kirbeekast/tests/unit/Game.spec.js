import { shallowMount, createLocalVue } from '@vue/test-utils'
import Game from '@/components/Game.vue'

import Vuex from 'vuex'
const localVue = createLocalVue()
localVue.use(Vuex)

describe('Game.vue', () => {
  it('Should have player only', () => {
    const store = new Vuex.Store({
      state: {
        'hasOpponent': false
      }
    })
    const wrapper = shallowMount(Game, { localVue, store })
    expect(wrapper.find('.player').exists()).toBe(true)
    expect(wrapper.find('.opponent').exists()).toBe(false)
  })

  it('Should have player and opponent', () => {
    const store = new Vuex.Store({
      state: {
        'hasOpponent': true
      }
    })
    const wrapper = shallowMount(Game, { localVue, store })
    expect(wrapper.find('.player').exists()).toBe(true)
    expect(wrapper.find('.opponent').exists()).toBe(true)
  })

  it('Should display round message', () => {
    const store = new Vuex.Store({
      state: {
        'roundMessage': 'THIS IS A TEST MESSAGE'
      }
    })
    const wrapper = shallowMount(Game, { localVue, store })
    expect(wrapper.find('.message-container').text()).toEqual('THIS IS A TEST MESSAGE')
  })

  it('Player should have win', () => {
    const store = new Vuex.Store({
      state: {
        'hasOpponent': true,
        'isPlayerWinner': true,
        'hasRoundEnded': true
      }
    })
    const wrapper = shallowMount(Game, { localVue, store })

    expect(wrapper.find('.player').classes('fight')).toBe(true)
    expect(wrapper.find('.player').classes('dead')).toBe(false)

    expect(wrapper.find('.opponent').classes('fight')).toBe(true)
    expect(wrapper.find('.opponent').classes('dead')).toBe(true)
  })

  it('Player should have lost', () => {
    const store = new Vuex.Store({
      state: {
        'hasOpponent': true,
        'isPlayerWinner': false,
        'hasRoundEnded': true
      }
    })
    const wrapper = shallowMount(Game, { localVue, store })

    expect(wrapper.find('.player').classes('fight')).toBe(true)
    expect(wrapper.find('.player').classes('dead')).toBe(true)

    expect(wrapper.find('.opponent').classes('fight')).toBe(true)
    expect(wrapper.find('.opponent').classes('dead')).toBe(false)
  })
})
