import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const constants = {
  MENU_SCREEN: 0,
  ROUND_SCREEN: 1
}

export default new Vuex.Store({
  state: {
    'countdown': null,
    'gameState': constants.MENU_SCREEN,
    'hasOpponent': false,
    'hasRoundEnded': false,
    'isPlayerWinner': false,
    'player': null,
    'roundMessage': 'AWAITING FOR YOUR MIGHTY OPPONENT',
    'roundState': null,
    'socket': null,
    'timer': null,
    'waitingForRoom': false
  },
  mutations: {
    socket (state, socket) {
      state.socket = socket
    },
    gameState (state, gameState) {
      state.gameState = gameState
    },
    roundState (state, roundState) {
      state.roundState = roundState
    },
    playerId (state, playerId) {
      state.playerId = playerId
    },
    countdown (state, countdown) {
      state.countdown = countdown
    },
    hasOpponent (state, hasOpponent) {
      state.hasOpponent = hasOpponent
    },
    isPlayerWinner (state, winnerId) {
      state.isPlayerWinner = winnerId === state.playerId
    },
    hasRoundEnded (state, hasRoundEnded) {
      state.hasRoundEnded = hasRoundEnded
    },
    cancelTimer (state) {
      clearTimeout(state.timer)
      state.timer = null
    },
    roundMessage (state, message) {
      state.roundMessage = message
    },
    waitingForRoom (state, waitingForRoom) {
      state.waitingForRoom = waitingForRoom
    }
  },
  actions: {
    init ({ commit, dispatch, state }) {
      const socket = new WebSocket('ws://' + window.location.hostname + ':15333')
      socket.onmessage = (event) => {
        try {
          const eventData = JSON.parse(event.data)
          commit('roundState', eventData.eventType)
          switch (eventData.eventType) {
            case 'PING':
              socket.send(JSON.stringify({ 'eventType': 'PING' }))
              break
            case 'PLAYER_INIT':
              commit('playerId', eventData.playerId)
              commit('gameState', constants.ROUND_SCREEN)
              break
            case 'ROUND_START':
              dispatch('startRound', eventData.countdown)
              break
            case 'ROUND_FIGHT':
              commit('roundMessage', '!')
              break
            case 'ROUND_ENDED':
              commit('isPlayerWinner', eventData.winner)
              commit('hasRoundEnded', true)
              setTimeout(() => {
                const reflexMessage = eventData.reflexTime ? `(${eventData.reflexTime}ms)` : ''
                const endMessage = state.isPlayerWinner ? `YOU WIN! ${reflexMessage}` : 'YOU LOSE!'
                commit('roundMessage', endMessage)
              }, 1500)
              break
            case 'ROOM_FULL':
              commit('waitingForRoom', true)
              setTimeout(() => dispatch('joinGame'), 2000)
              break
            case 'BACK_TO_MENU':
              dispatch('resetRound')
              commit('gameState', 0)
              break
          }
        } catch (err) {
          dispatch('resetRound')
          commit('gameState', 0)
        }
      }
      document.addEventListener('touchstart', function () {
        socket.send(JSON.stringify({ 'input': 'space' }))
      }, false)
      document.body.onkeydown = function (e) {
        if (e.keyCode === 32) socket.send(JSON.stringify({ 'input': 'space' }))
      }
      commit('socket', socket)
    },
    joinGame ({ state }) {
      state.socket.send(JSON.stringify({ 'event': 'joining_attempt' }))
    },
    resetRound ({ commit }) {
      commit('cancelTimer')
      commit('roundMessage', 'AWAITING FOR YOUR MIGHTY OPPONENT') // TODO constants
      commit('isPlayerWinner', false)
      commit('hasRoundEnded', false)
      commit('hasOpponent', false)
      commit('waitingForRoom', false)
    },
    startRound ({ commit, dispatch, state }, countdown) {
      commit('countdown', countdown)
      commit('roundMessage', state.countdown)
      commit('hasOpponent', true)
      dispatch('updateCountdown')
    },
    updateCountdown ({ commit, dispatch, state }) {
      if (state.countdown <= 0) return
      state.timer = setTimeout(() => {
        commit('countdown', state.countdown - 1)
        commit('roundMessage', state.countdown)
        dispatch('updateCountdown')
      }, 1000)
    }
  }
})
