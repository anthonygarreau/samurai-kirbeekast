const TIMES = {
  BACK_TO_MENU: 5000,
  PING_MARGIN: 200,
  COUNTDOWN: 3,
  MAX_WAIT_TIME: 6,
  MIN_WAIT_TIME: 2
}

class Room {
	constructor() {
		this.players = []
    this.round = null,
    this.roomState = 'WAITING_FOR_PLAYERS';
	}

	handlePlayerConnection(connection) {
		const player = {
      'connection': connection,
      'id': connection.uid
    }
		this.initPlayerEvents(player)
	}

	initPlayerEvents(player) {
		player.connection.on('message', (event) => {
			if (event.type === 'utf8') {
				try {
					const eventData = JSON.parse(event.utf8Data)
          if (eventData.event === 'joining_attempt')  {
            this.playerJoinRoom(player)
          } else if (eventData.eventType === 'PING') {
            this.calculatePlayerPing(player)
          } else if (eventData.input) {
            this.checkInput(player);
          }
				} catch (err) {
					console.error('Error: ', err)
				}
			}
		})

		player.connection.on('close', () => {
      if (!this.players.find((p) => p.id === player.id)) return;
      this.broadcast({ 'eventType': 'BACK_TO_MENU' })
      this.resetRoom()
		})
  }
  
  playerJoinRoom(player) {
    if (this.players.length >= 2) {
      player.connection.send(JSON.stringify({ 'eventType': 'ROOM_FULL' }))
      return
    }
    
    player.roundInfo = {}
    this.players.push(player);

    player.connection.send(JSON.stringify({  // TODO Refacto
			'eventType': 'PLAYER_INIT',
			'playerId': player.id,
			'roomState': this.roomState,
		}));

		// if both player have joined
		if (this.players.length === 2) {
      this.getPlayersPing();
			this.startRound();
		}
  }

	startRound() {
    const waitingTime = (Math.floor(Math.random() * (TIMES.MAX_WAIT_TIME + 1 - TIMES.MIN_WAIT_TIME)) + TIMES.MIN_WAIT_TIME) * 1000
    this.roomState = 'ROUND_START';
    this.broadcast({'eventType': this.roomState, 'countdown': TIMES.COUNTDOWN})

		this.round = {
      waitingTime,
      waitTimer: this.startRoundWaitTimer(),
      fightTimer: this.startRoundFightTimer(waitingTime)
		}
	}

	checkInput(player) {
		if (this.roomState !== 'ROUND_FIGHT' && this.roomState !== 'ROUND_WAIT') return
		
		if (this.roomState === 'ROUND_WAIT') {
      // If a player triggered too soon before fight state, loses
      this.roomState = 'ROUND_ENDED'
			this.broadcast({
				'eventType': this.roomState,
				'winner': this.players.find((p) => p.id != player.id).id,
      })
      this.startBackToMenuTimer()
		} else if (this.roomState === 'ROUND_FIGHT') {
      player.roundInfo.triggerTime = Date.now()
      this.startWinCheckTimer()
    }
  }

  startRoundWaitTimer() {
    return setTimeout(() => {
      this.roomState = 'ROUND_WAIT'
      this.broadcast({'eventType': this.roomState})
    }, TIMES.COUNTDOWN * 1000)
  }

  startRoundFightTimer(waitingTime) {
    return setTimeout(() => {
      if (this.roomState === 'ROUND_ENDED') return

      this.roomState = 'ROUND_FIGHT'
      this.round.roundStartTime = Date.now()
      this.broadcast({'eventType': this.roomState})
    }, waitingTime + TIMES.COUNTDOWN * 1000)
  }

  startWinCheckTimer() {
    // If a player triggered during fight round
    // Wait for highest ping value in room
    // And get winner according to each player corrected reflex time
    if (this.round.winCheckTimer) return;
    this.round.winCheckTimer = setTimeout(() => {
      const winner = this.getFastestPlayer()
      this.roomState = 'ROUND_ENDED'
      this.broadcast({
        'eventType': this.roomState,
        'winner': winner.id,
        'reflexTime': this.getCorrectedReflexTime(winner)
      })

      this.startBackToMenuTimer()
    }, this.getPlayersHighestPing() + TIMES.PING_MARGIN)
  }

  startBackToMenuTimer() {
    this.round.backToMenuTimer = setTimeout(() => { 
      this.broadcast({ 'eventType': 'BACK_TO_MENU' })
      this.resetRoom()
    }, TIMES.BACK_TO_MENU)
  }
  
  resetRoom() {
    if (this.round) {
      clearTimeout(this.round.waitTimer)
      clearTimeout(this.round.fightTimer)
      clearTimeout(this.round.winCheckTimer)
      clearTimeout(this.round.backToMenuTimer)
    }

    this.players = []
    this.round = null
    this.roomState = 'WAITING_FOR_PLAYERS'
  }

  getPlayersPing() {
    this.players.forEach(player => {
      player.roundInfo.startPingTime = Date.now();
      player.connection.send(JSON.stringify({ 'eventType': 'PING' }))
		})
  }

  getPlayersHighestPing() {
    const [player1, player2] = this.players;
    return player1.roundInfo.ping > player2.roundInfo.ping ? player1.roundInfo.ping : player2.roundInfo.ping
  }

  getFastestPlayer() {
    const [player1, player2] = this.players;
    if (player1.roundInfo.triggerTime && !player2.roundInfo.triggerTime) return player1
    else if (!player1.roundInfo.triggerTime && player2.roundInfo.triggerTime) return player2
    else if (this.getCorrectedReflexTime(player1) < this.getCorrectedReflexTime(player2)) return player1
    else if (this.getCorrectedReflexTime(player1) > this.getCorrectedReflexTime(player2)) return player2
  }

  getCorrectedReflexTime(player) {
    return player.roundInfo.triggerTime - this.round.roundStartTime - player.roundInfo.ping
  }

  calculatePlayerPing(player) {
    player.roundInfo.stopPingTime = Date.now();
    player.roundInfo.ping = player.roundInfo.stopPingTime - player.roundInfo.startPingTime
  }

	broadcast(event) {
		this.players.forEach(player => {
			player.connection.send(JSON.stringify(event))
		})
	}
}

module.exports = Room;
