process.title = 'samurai-server'

const http = require('http')
const webSocketServer = require('websocket').server
const webSocketsServerPort = 15333
const Room = require('./room.js');
const baseRoom = new Room();

const httpServer = http.createServer();
httpServer.listen(webSocketsServerPort, () => {
	console.log("Server is listening on port " + webSocketsServerPort)
});

const wsServer = new webSocketServer({ httpServer })

wsServer.on('request', function(request) {

	const connection = request.accept(null, request.origin)
	connection.uid = Math.random().toString() // TODO lazy id, replace by real uuid

	try {
		baseRoom.handlePlayerConnection(connection)
	} catch(err) {
		console.error('Error on player connection: ', err.message)
		connection.send(JSON.stringify(err));
	}

})