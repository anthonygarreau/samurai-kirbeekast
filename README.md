# Samurai Kirbeekast

## How to play on local
1. Start Server
```bash
cd samurai-server
npm i
npm run serve
```
2. Start App
```bash
cd samurai-kirbeekast
npm i
npm run serve
```
3. Go on [http://localhost:8080](http://localhost:8080)
4. Find a friend (if you dont have any, look for a coworker) to play with

## How is this played ?
1. Click on the "Play" button
2. Wait for another player to come
3. When the "!" symbol appears, press spacebar/tap like a real ninja

## How is the ping handled ?
Server takes in account players ping during a round doing the following: 
1. During round start, server ping both players and stores both pings durations
2. When a player press SPACEBAR, server waits for the highest player ping duration for the other request
3. If second player doesn't request in time, first player wins
4. If second player requests in time, servers does the following :
 - For both players, calculate duration as :
 "round start time - (player request time - players ping)"
 - player with lower duration wins.

If this isnt clear, just ask me, i will be happy to draw something on a sheet of paper ^_^

to Beekast dev team : Please note that i've now become paranoid about my reaction time and if this algorithm is really accurate.
From: someone playing with this https://www.estopwatch.net/ right now


## Why is the logo so ugly ?
~~I am sorry~~

## TODO

* [x] Basic Scene (ui)
* [x] Basic Room (backend)
* [x] Communication
* [x] Round states
* [x] Winner management
* [x] Main Menu / Game reset
* [x] Animations
* [x] Sounds
* [x] Waiting time when room is full 
* [x] Network Input Lag handling
* [ ] Draw / Quick Death / Close Fight animations
* [ ] Allow multiple rooms
* [ ] Multiple characters skins
* [ ] Multiple backgrounds
* [ ] Login
* [ ] ELO system
* [ ] This is going too far ... but this game is fun ^_^